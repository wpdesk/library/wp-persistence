[![pipeline status](https://gitlab.com/wpdesk/wp-persistence/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-persistence/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-persistence/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wp-persistence/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-persistence/v/stable)](https://packagist.org/packages/wpdesk/wp-persistence) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-persistence/downloads)](https://packagist.org/packages/wpdesk/wp-persistence) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-persistence/v/unstable)](https://packagist.org/packages/wpdesk/wp-persistence) 
[![License](https://poser.pugx.org/wpdesk/wp-persistence/license)](https://packagist.org/packages/wpdesk/wp-persistence)

WordPress Library to facilitate access to various persistence possibilities
===================================================

## Requirements

PHP 7.0 or later.

## Composer

You can install the bindings via [Composer](http://getcomposer.org/). Run the following command:

```bash
composer require wpdesk/wp-persistence
```

To use the bindings, use Composer's [autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading):

```php
require_once 'vendor/autoload.php';
```

## Compatiblity between plugins

To ensure that always the latest and valid version of composer libraries are loaded in WP env you should use a solution
that ensure support between plugins and at least decreases the change that something would break. At the moment we recommend
using wpdesk/wp-autoloader.


## Manual instalation

If you do not wish to use Composer and wpdesk/wp-autoloader, you probably should stop using any existing library as it breaks compatibility between plugins.

## When _has()_ is true and when _get()_ will throw an exception

|  Value | Get        | Has        |
|--------|------------|------------|
| 'test' | 'test'     | true       |
| []     | []         | true       |
| ''     | ''         | true       |
| 99     | '99' or 99 | true       |
| 0      | '0' or 0   | true       |
| true   | true or '1'| true       |
| false  | false or ''| true       |
| not set| exception  | false      |
| null   | exception  | false      | 
