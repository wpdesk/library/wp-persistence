<?php

use WPDesk\Persistence\Adapter\ReferenceArrayContainer;

class TestReferenceArrayContainer extends \PHPUnit\Framework\TestCase {
	const SOME_KEY = 'some_key';
	const SOME_VALUE = 'some_value';

	public function test_can_use_reference() {
		$referenced_array = [];
		$container        = new ReferenceArrayContainer( $referenced_array );

		$container->set( self::SOME_KEY, self::SOME_VALUE );
		$this->assertEquals( self::SOME_VALUE, $referenced_array[ self::SOME_KEY ] );
	}

}