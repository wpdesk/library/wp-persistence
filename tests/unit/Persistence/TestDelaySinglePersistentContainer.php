<?php

use WPDesk\Persistence\Decorator\DelaySinglePersistentContainer;
use WPDesk\Persistence\PersistentContainer;

class TestDelaySinglePersistentContainer extends \PHPUnit\Framework\TestCase {
	const SOME_MASTER_KEY = 'some_master_key';
	const SOME_KEY = 'some_key';
	const SOME_VALUE = 'some_value';

	public function test_add_value() {
		@$persistent_container = $this->createMock( PersistentContainer::class );
		$delay_single_persistent_container = new DelaySinglePersistentContainer( $persistent_container, self::SOME_MASTER_KEY );
		$delay_single_persistent_container->set( self::SOME_KEY, self::SOME_VALUE );

		$all_data = $delay_single_persistent_container->get_all();
		$this->assertEquals( self::SOME_VALUE, $all_data[ self::SOME_KEY ] );
	}

}