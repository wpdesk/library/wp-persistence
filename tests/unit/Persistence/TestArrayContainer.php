<?php

use WPDesk\Persistence\ElementNotExistsException;
use WPDesk\Persistence\Adapter\ArrayContainer;

class TestArrayContainer extends \PHPUnit\Framework\TestCase {
	const SOME_KEY = 'some_key';
	const SOME_VALUE = 'some_value';

	public function test_can_get_what_is_set() {
		$container = new ArrayContainer();

		$container->set( self::SOME_KEY, self::SOME_VALUE );
		$this->assertTrue( $container->has( self::SOME_KEY ) );
		$this->assertEquals( self::SOME_VALUE, $container->get( self::SOME_KEY ) );
	}

	public function test_can_truly_delete() {
		$container = new ArrayContainer();

		// firstly set and check if really set
		$container->set( self::SOME_KEY, self::SOME_VALUE );
		$this->assertTrue( $container->has( self::SOME_KEY ) );
		$this->assertEquals( self::SOME_VALUE, $container->get( self::SOME_KEY ) );

		// delete and check if no value
		$container->delete( self::SOME_KEY );
		$this->expectException( ElementNotExistsException::class );
		$this->assertFalse( $container->has( self::SOME_KEY ) );
		$container->get( self::SOME_KEY );
	}

	public function test_can_init() {
		$init_array = [ self::SOME_KEY => self::SOME_VALUE ];
		$container  = new ArrayContainer( $init_array );

		$this->assertTrue( $container->has( self::SOME_KEY ) );
		$this->assertEquals( self::SOME_VALUE, $container->get( self::SOME_KEY ) );
	}

	public function test_throws_exception_when_not_set() {
		$container = new ArrayContainer();
		$this->expectException( ElementNotExistsException::class );

		$this->assertFalse( $container->has( self::SOME_KEY ) );
		$container->get( self::SOME_KEY );
	}
}