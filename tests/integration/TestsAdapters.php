<?php

namespace WPDesk\Persistence\Tests\Integration;

use WPDesk\Persistence\Adapter\ArrayContainer;
use WPDesk\Persistence\Adapter\WooCommerce\WooCommerceSessionContainer;
use WPDesk\Persistence\Adapter\WordPress\WordpressSerializedOptionsContainer;
use WPDesk\Persistence\Decorator\DelayPersistentContainer;
use WPDesk\Persistence\Adapter\ReferenceArrayContainer;
use WPDesk\Persistence\Adapter\WooCommerce\WooCommerceSettingsContainer;
use WPDesk\Persistence\Adapter\WooCommerce\WooCommerceShippingInstanceContainer;
use WPDesk\Persistence\Adapter\WordPress\WordpressOptionsContainer;
use WPDesk\Persistence\Adapter\WordPress\WordpressPostMetaContainer;
use WPDesk\Persistence\Adapter\WordPress\WordpressTransientContainer;
use WPDesk\Persistence\Decorator\DelaySinglePersistentContainer;
use WPDesk\Persistence\Decorator\SerializedPersistentContainer;
use WPDesk\Persistence\ElementNotExistsException;
use WPDesk\Persistence\PersistentContainer;

class TestsAdapters extends \PHPUnit\Framework\TestCase {
	const SOME_KEY = 'some_key';
	const SOME_VALUE = 'some_value';

	/**
	 * @return PersistentContainer[]
	 */
	public function adapters_to_test_provider() {
		$some_array_to_be_referenced = [];

		return [
			[ new ArrayContainer() ],
			[ new ReferenceArrayContainer( $some_array_to_be_referenced ) ],
			[ new WordpressOptionsContainer() ],
			[ new WordpressPostMetaContainer( 1 ) ],
			[ new WordpressSerializedOptionsContainer( 'option_name' ) ],
			[ new WordpressTransientContainer() ],
			[ new WooCommerceSettingsContainer( new \WC_Gateway_BACS() ) ],
			[ new WooCommerceShippingInstanceContainer( new \WC_Shipping_Flat_Rate() ) ],
			[ new WooCommerceSessionContainer( WC()->session ) ],
			[ new DelayPersistentContainer( new ArrayContainer() ) ],
			[ new DelaySinglePersistentContainer( new ArrayContainer(), 'some_key' ) ],
			[ new SerializedPersistentContainer( new ArrayContainer() ) ]
		];
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_null_set_is_delete( PersistentContainer $container ) {
		$container->set( self::SOME_KEY, null );
		$this->assertFalse( $container->has( self::SOME_KEY ), "Null is like delete so ::has() should return false" );

		$this->expectException( ElementNotExistsException::class );
		$container->get( self::SOME_KEY, "For null ::get() should throw exception" );
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_empty_string_set_is_not_delete( PersistentContainer $container ) {
		$this->check_value_that_not_exception( $container, '' );
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_false_set_is_not_delete( PersistentContainer $container ) {
		$this->check_value_that_not_exception( $container, false );
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_empty_array_set_is_not_delete( PersistentContainer $container ) {
		$this->check_value_that_not_exception( $container, [] );
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_true_set_is_not_delete( PersistentContainer $container ) {
		$this->check_value_that_not_exception( $container, true );
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_number_set_is_not_delete( PersistentContainer $container ) {
		$this->check_value_that_not_exception( $container, 99 );
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_empty_number_set_is_not_delete( PersistentContainer $container ) {
		$this->check_value_that_not_exception( $container, 0 );
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_throws_exception_when_not_set( PersistentContainer $container ) {
		$this->expectException( ElementNotExistsException::class );
		$container->delete( self::SOME_KEY );
		$this->assertFalse( $container->has( self::SOME_KEY ) );
		$container->get( self::SOME_KEY );
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_can_get_what_is_set( PersistentContainer $container ) {
		$container->set( self::SOME_KEY, self::SOME_VALUE );
		$this->assertTrue( $container->has( self::SOME_KEY ) );
		$this->assertEquals( self::SOME_VALUE, $container->get( self::SOME_KEY ) );
	}

	/**
	 * @dataProvider adapters_to_test_provider
	 */
	public function test_can_delete( PersistentContainer $container ) {
		// firstly set and check if really set
		$container->set( self::SOME_KEY, self::SOME_VALUE );
		$this->assertTrue( $container->has( self::SOME_KEY ) );
		$this->assertEquals( self::SOME_VALUE, $container->get( self::SOME_KEY ) );

		// delete and check if no value

		$container->delete( self::SOME_KEY );
		$this->expectException( ElementNotExistsException::class );
		$this->assertFalse( $container->has( self::SOME_KEY ) );
		$container->get( self::SOME_KEY );

		// check if fallback works and does not use exceptions
		$this->assertEquals( $container->get_fallback( self::SOME_KEY, self::SOME_VALUE ), self::SOME_VALUE );

	}

	public function test_can_init_array_container() {
		$init_array = [ self::SOME_KEY => self::SOME_VALUE ];
		$container  = new ArrayContainer( $init_array );

		$this->assertTrue( $container->has( self::SOME_KEY ) );
		$this->assertEquals( self::SOME_VALUE, $container->get( self::SOME_KEY ) );
	}

	private function check_value_that_not_exception( PersistentContainer $container, $value ) {
		$container->set( self::SOME_KEY, $value );
		$this->assertTrue( $container->has( self::SOME_KEY ), 'Value has been set so ::has() should return true' );
		$this->assertEquals( $value, $container->get( self::SOME_KEY ),
			"Value has been set so ::get() should return the value" ); // do not use assertEquals as it cast false to ''

		$this->assertEquals( $container->get_fallback( self::SOME_KEY ), $value );
	}
}